import { Container } from "../../styles/table";
import { IoIosArrowDown } from "react-icons/io";
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  FormControlLabel,
} from "@material-ui/core";
import ModalEditActivities from "../../components/ModalEditActivities";
import { useState } from "react";
import Button from "./../Button";

const TableGroupActivities = ({ content, callbackDelete, callbackUpdate }) => {
  const [open, setOpen] = useState(false);

  return (
    <Container>
      {content.map((item) => (
        <Accordion key={item.id}>
          <AccordionSummary
            expandIcon={<IoIosArrowDown />}
            aria-label="Expand"
            aria-controls="additional-actions1-content"
            id="additional-actions1-header"
          >
            {open && (
              <ModalEditActivities
                setOpen={setOpen}
                callbackUpdate={callbackUpdate}
                idActivitie={item.id}
              />
            )}
            <FormControlLabel
              aria-label="Acknowledge"
              onClick={(event) => event.stopPropagation()}
              onFocus={(event) => event.stopPropagation()}
              control={<Button onClick={() => setOpen(true)}>Editar</Button>}
              label={item.title}
            />
          </AccordionSummary>
          <AccordionDetails>
            <p>
              <b>Realização: </b>
              <span>{item.realization_time}</span>
            </p>
            <Button onClick={() => callbackDelete(item.id)}>Deletar</Button>
          </AccordionDetails>
        </Accordion>
      ))}
    </Container>
  );
};

export default TableGroupActivities;
