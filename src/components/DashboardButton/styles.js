import styled from "styled-components";

export const DashboardButtonStyle = styled.button`
  background-color: transparent;

  border: none;
  height: 24px;
  font-size: 20px;
  display: flex;

  align-items: center;

  justify-content: center;
  font-family: "Alegreya Sans SC";
  div {
    margin-left: 4px;
    display: flex;
    align-items: center;
  }
`;
