import { BiPlusCircle } from "react-icons/bi";
import { DashboardButtonStyle } from "./styles";

const DashboardButton = ({ children, funcao }) => {
  return (
    <>
      <DashboardButtonStyle onClick={funcao}>
        {children}
        <div>
          <BiPlusCircle />
        </div>
      </DashboardButtonStyle>
    </>
  );
};
export default DashboardButton;
