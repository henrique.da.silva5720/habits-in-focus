import { Container } from "../../styles/table";

import { IoIosArrowDown } from "react-icons/io";
import { useAuth } from "../../hooks/Auth";

import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  FormControlLabel,
} from "@material-ui/core";

import Button from "../Button";

import { api } from "../../services/api";

import { toast } from "react-toastify";

import { useHistory } from "react-router-dom";

const TableGroup = ({ groupList, name, button = false }) => {
  const { auth } = useAuth();
  const history = useHistory();

  const joinGroup = (id) => {
    api
      .post(
        `/groups/${id}/subscribe/`,
        {},
        {
          headers: {
            Authorization: `Bearer ${auth}`,
          },
        }
      )
      .then((_) => toast.success("Entrou no grupo!"))
      .catch((_) => toast.error("Erro ao entrar no grupo!"));
  };

  const visitarAtiv = (idGroup) => {
    history.push(`/dashboard/groups/goals:${idGroup}`); // verificar se rota está certa
  };

  return (
    <Container>
      {groupList &&
        groupList.map((group) => (
          <Accordion key={group.id}>
            <AccordionSummary
              expandIcon={<IoIosArrowDown />}
              aria-label="Expand"
              aria-controls="additional-actions1-content"
              id="additional-actions1-header"
            >
              <FormControlLabel
                aria-label="Acknowledge"
                onClick={(event) => event.stopPropagation()}
                onFocus={(event) => event.stopPropagation()}
                control={
                  <Button
                    myGroup={false}
                    onClick={() => {
                      if (name === "Entrar") {
                        joinGroup(group.id);
                      }
                    }}
                  >
                    {name}
                  </Button>
                }
                label={group.name}
              />
            </AccordionSummary>
            <AccordionDetails>
              <p>
                <b>Categoria:</b> {group.category}
              </p>
              <p>
                <b>Descrição:</b> {group.description}
              </p>
              {button && (
                <Button onClick={() => visitarAtiv(group.id)}>Visitar</Button>
              )}
            </AccordionDetails>
          </Accordion>
        ))}
    </Container>
  );
};

export default TableGroup;
