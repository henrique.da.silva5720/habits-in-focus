import { Container } from "../../styles/table";
import { IoIosArrowDown } from "react-icons/io";
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  FormControlLabel,
} from "@material-ui/core";

import Button from "./../Button";

const TableGroupGoal = ({ content, callbackDelete, callbackUpdate }) => {
  return (
    <Container>
      {content.map((item) => (
        <Accordion key={item.id}>
          <AccordionSummary
            expandIcon={<IoIosArrowDown />}
            aria-label="Expand"
            aria-controls="additional-actions1-content"
            id="additional-actions1-header"
          >
            <FormControlLabel
              aria-label="Acknowledge"
              onClick={(event) => event.stopPropagation()}
              onFocus={(event) => event.stopPropagation()}
              control={
                <Button onClick={() => callbackUpdate(item.id, item.achieved)}>
                  {item.achieved ? "Concluido" : "Completar"}
                </Button>
              }
              label={item.title}
            />
          </AccordionSummary>
          <AccordionDetails>
            <p>
              <b>Dificuldade: </b>
              <span>{item.difficulty}</span>
            </p>
            <p>
              <b>Complemento:</b> {item.how_much_achieved}
            </p>
            <p>
              <b>Estado:</b> {item.achieved ? "Completo" : "Não Completo"}
            </p>
            <Button onClick={() => callbackDelete(item.id)}>Deletar</Button>
          </AccordionDetails>
        </Accordion>
      ))}
    </Container>
  );
};

export default TableGroupGoal;
