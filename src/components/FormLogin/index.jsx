import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { toast } from "react-toastify";
import { useHistory } from "react-router-dom";
import { api } from "../../services/api";
import Input from "../Input";
import Button from "../Button";
import { BiUserCircle, BiLockAlt } from "react-icons/bi";
import { useAuth } from "../../hooks/Auth";
import { Form } from "../../styles/form";

const FormLogin = () => {
  const history = useHistory();

  const { setAuth } = useAuth();

  const formSchema = yup.object().shape({
    username: yup.string().required("Campo obrigatório"),
    password: yup.string().required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(formSchema) });

  const handleFormLogin = (data) => {
    api
      .post("/sessions/", data)
      .then((response) => {
        localStorage.clear();
        localStorage.setItem("token", response.data.access);
        setAuth(response.data.access);
        history.push("/dashboard/user");
      })
      .catch((err) => toast.error("Nome de usuário ou senha inválida!"));
  };

  return (
    <Form onSubmit={handleSubmit(handleFormLogin)}>
      <h1>Login</h1>
      <Input
        label="Nome de usuário"
        icon={BiUserCircle}
        name="username"
        register={register}
        error={errors.username?.message}
        placeholder="Nome  de usuário"
      />
      <Input
        label="Senha"
        icon={BiLockAlt}
        name="password"
        register={register}
        error={errors.password?.message}
        placeholder="Senha"
        type="password"
      />
      <Button type="submit">Login</Button>
    </Form>
  );
};

export default FormLogin;
