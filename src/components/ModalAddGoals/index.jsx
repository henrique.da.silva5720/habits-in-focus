import { ModalCont, closeContainer, buttonContainer } from "../../styles/modal";

import Button from "../Button";
import Input from "../Input";
import CloseIcon from "@material-ui/icons/Close";

import { useAuth } from "../../hooks/Auth";

import { useForm } from "react-hook-form";

import { toast } from "react-toastify";

import { api } from "../../services/api";

import { useGroup } from "../../hooks/Group";

const ModalAddGoals = ({ setOpen, idGroup, nameGrupo }) => {
  // o setOpen é usado para fechar o modal, então deve ser criado um booleano para usar no handleClick do botão que chama o modal com o ternário.
  // necessário ser chamado  esse modal do id do grupo e do nome do grupo que o goal será criado

  const { getGroupGoals } = useGroup();

  const { auth } = useAuth();

  const handleClose = () => {
    setOpen(false);
  };

  const { register, handleSubmit } = useForm();

  const handleGroups = (data) => {
    const newGoalsData = {
      ...data,
      how_much_achieved: 0,
      group: idGroup,
    };

    let config = {
      headers: {
        Authorization: `Bearer ${auth}`,
      },
    };
    api
      .post("/goals/", newGoalsData, config)
      .then((response) => {
        getGroupGoals(idGroup);
        toast.success("Cadastro com sucesso");
      })
      .catch((err) => toast.error("Erro ao Cadastrar"));
  };

  return (
    <ModalCont>
      <div className="conteiner">
        <div style={closeContainer}>
          <CloseIcon
            onClick={handleClose}
            style={{ marginRight: 10, marginTop: 5 }}
          />
        </div>
        <p style={{ width: "80%", margin: "0 auto" }}>
          Adicionar Meta ao grupo{nameGrupo}:
        </p>
        <div style={buttonContainer}>
          <form onSubmit={handleSubmit(handleGroups)}>
            <Input
              label="Title"
              name="title"
              register={register}
              placeholder="Titulo"
            />
            <Input
              label="Dificuldade"
              name="difficulty"
              register={register}
              placeholder="Dificil, fácil ou muito dificil"
            />
            <Button type="submit"> Adicionar </Button>
          </form>
        </div>
      </div>
    </ModalCont>
  );
};

export default ModalAddGoals;
