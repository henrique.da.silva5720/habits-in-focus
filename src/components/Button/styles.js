import styled from "styled-components";

export const Container = styled.button`
  width: 150px;
  height: 45px;

  color: var(--violet);
  visibility: ${(prop) => prop.myGroup && "hidden"};

  background-color: var(--white);
  border: 2px solid var(--violet);
  border-radius: 5px;

  transition: 0.3s;

  :hover {
    color: var(--white);

    background-color: var(--violet);
  }
`;
