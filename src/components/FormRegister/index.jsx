// Importando tudo que será usado no formulário
import * as yup from "yup";
import { useHistory } from "react-router";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { api } from "../../services/api";
// Importando todos ícones
import { BiLockAlt, BiUserCircle } from "react-icons/bi";
import { FiMail } from "react-icons/fi";

// Importando components
import Button from "../Button";
import Input from "../Input/index";
import { Form } from "../../styles/form";

import { toast } from "react-toastify";

const FormRegister = () => {
  const history = useHistory();
  const schema = yup.object().shape({
    username: yup
      .string()
      .required("Campo obrigatório")
      .matches(/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$/, "Somente letras"),

    email: yup.string().required("Campo obrigatório").email("Email inválido"),
    password: yup
      .string()
      .required("Campo obrigatório")
      .min(6, "Minimo 6 caractéres")
      .matches(
        /^((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
        "Requer: letra maiúscula, minúscula, número, caracter especial"
      ),
    verifyPassword: yup
      .string()
      .required("Campo obrigatório")
      .oneOf([yup.ref("password")], "As Senhas não correspondem"),
  });
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });
  const handleForm = ({ username, email, password }) => {
    const data = { username, email, password };
    api
      .post("/users/", data)
      .then((res) => {
        history.push("/login");
        toast.success("Cadastro com sucesso");
      })
      .catch((err) => toast.error("Usuário já existente"));
  };
  return (
    <Form onSubmit={handleSubmit(handleForm)}>
      <h1>Cadastro</h1>

      <Input
        error={errors.username?.message}
        name="username"
        label="Nome de usuário"
        placeholder="Nome de usuário"
        icon={BiUserCircle}
        register={register}
      />
      <Input
        error={errors.email?.message}
        name="email"
        label="Email"
        placeholder="Seu melhor email"
        icon={FiMail}
        register={register}
      />
      <Input
        error={errors.password?.message}
        label="Senha"
        placeholder="Sua senha mais segura"
        name="password"
        type="password"
        icon={BiLockAlt}
        register={register}
      />
      <Input
        error={errors.verifyPassword?.message}
        label="Confirme sua senha"
        placeholder="Confirmação de senha"
        name="verifyPassword"
        type="password"
        icon={BiLockAlt}
        register={register}
      />

      <Button type="submit">Enviar</Button>
    </Form>
  );
};

export default FormRegister;
