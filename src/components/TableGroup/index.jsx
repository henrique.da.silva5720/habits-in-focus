import { Container } from "../../styles/table";
import { IoIosArrowDown } from "react-icons/io";
import { useAuth } from "../../hooks/Auth";

import TextField from "@material-ui/core/TextField";

import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  FormControlLabel,
} from "@material-ui/core";

import Button from "../Button";

import { api } from "../../services/api";

import { toast } from "react-toastify";

import { useHistory } from "react-router-dom";
import { useState } from "react";

const TableGroupEdit = ({ groupList, name, getGroupAction }) => {
  const { auth, getUserId } = useAuth();
  const [idgroup, setIdgroup] = useState("");
  const history = useHistory();
  const [category, setCategory] = useState("");
  const idUsuer = getUserId();

  const visitarAtiv = (idGroup) =>
    history.push(`/dashboard/user/group/${idGroup}`); // verificar se rota está certa;

  const handleKeyPress = async (e) => {
    if (e.key === "Enter") {
      await updateCategory(category);
    }
  };
  const updateCategory = async (data) => {
    const newUpdateData = {
      category: `@habitsinfocus/${data}`,
    };

    let config = {
      headers: {
        Authorization: `Bearer ${auth}`,
      },
    };

    api
      .patch(`/groups/${idgroup}/`, newUpdateData, config)
      .then(() => {
        setIdgroup("");
        toast.success("Atualizado com sucesso");
        getGroupAction();
      })
      .catch(() => toast.error("Erro ao Cadastrar"));
  };

  return (
    <Container>
      {groupList &&
        groupList.map((group) => (
          <Accordion key={group.id}>
            <AccordionSummary
              expandIcon={<IoIosArrowDown />}
              aria-label="Expand"
              aria-controls="additional-actions1-content"
              id="additional-actions1-header"
            >
              <FormControlLabel
                aria-label="Acknowledge"
                onClick={(event) => event.stopPropagation()}
                onFocus={(event) => event.stopPropagation()}
                control={
                  <Button
                    myGroup={!(group.creator?.id === idUsuer)}
                    onClick={() => {
                      if (group.creator.id === idUsuer) {
                        setIdgroup(group.id);
                      }
                    }}
                  >
                    {name}
                  </Button>
                }
                label={group.name}
              />
            </AccordionSummary>
            <AccordionDetails>
              <p>
                <b>Categoria: </b>
                {group.id === idgroup ? (
                  <TextField
                    id="filled-size-small"
                    defaultValue={group.category}
                    size="small"
                    InputProps={{
                      readOnly: false,
                    }}
                    onKeyPress={(e) => handleKeyPress(e, group)}
                    onChange={(e) => setCategory(e.target.value)}
                  />
                ) : (
                  <span>{group.category}</span>
                )}
              </p>
              <p>
                <b>Descrição:</b> {group.description}
              </p>
              {<Button onClick={() => visitarAtiv(group.id)}>Visitar</Button>}
            </AccordionDetails>
          </Accordion>
        ))}
    </Container>
  );
};

export default TableGroupEdit;
