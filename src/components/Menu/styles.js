import styled from "styled-components";

export const Conteiner = styled.div`
  display: flex;
`;

export const MenuContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  height: 100%;
  .menu span {
    text-align: center;
  }
`;
