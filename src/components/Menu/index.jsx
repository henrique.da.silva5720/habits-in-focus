import { Conteiner, MenuContainer } from "../Menu/styles";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import MenuIcon from "@material-ui/icons/Menu";
import Toolbar from "@material-ui/core/Toolbar";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import { GoPencil } from "react-icons/go";
import { BiLogOut } from "react-icons/bi";
import { api } from "../../services/api";
import { useAuth } from "../../hooks/Auth";

import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import TextField from "@material-ui/core/TextField";
import CircularProgress from "@material-ui/core/CircularProgress";
import { toast } from "react-toastify";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up("sm")]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
    backgroundColor: "#A293FF",
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  // necessary for content to be below app bar

  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const Menu = (props) => {
  const { window } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = useState(false);
  const history = useHistory();
  const [user, setUser] = useState("");
  const { getUserId, auth, setAuth } = useAuth();
  const [isEnable, setIsEnable] = useState(false);
  const [userId, setUserId] = useState("");

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const handleRoute = (route) => history.push(route);

  const handleChange = async (e) => {
    if (e.key === "Enter") {
      await updateUsename(user, auth, userId);

      setIsEnable(false);
    }
  };

  const updateUsename = async (user, auth, userId) => {
    let body = {
      username: user,
    };
    let config = {
      headers: {
        Authorization: `Bearer ${auth}`,
      },
    };
    await api
      .patch(`users/${userId}/`, body, config)
      .then(() => toast.success("Atualizado com sucesso"))
      .catch(() => toast.error("Erro ao atualizar"));
  };

  const updateUser = async () => {
    let user_id = getUserId();
    setUserId(user_id);
    const response = await api.get(`/users/${user_id}/`);
    setUser(response.data.username);
  };

  useEffect(() => {
    updateUser();
    // eslint-disable-next-line
  }, []);

  const respMenu = (
    <div className="menu">
      <List>
        {[
          { route: "/dashboard/user", text: "Meus Hábitos" },
          { route: "/dashboard/groups", text: "Meus Grupos" },
          { route: "/dashboard/search", text: "Buscar Grupo" },
        ].map((menu, index) => (
          <ListItem
            onClick={() => {
              handleRoute(menu.route);
            }}
            button
            key={index}
          >
            <ListItemText primary={menu.text} />
          </ListItem>
        ))}
      </List>
    </div>
  );

  const welcomeUser = (
    <div>
      <div className={classes.toolbar} />
      <Divider />
      <List>
        <ListItem>
          <ListItemText primary="Bem-vindo," />
        </ListItem>
        <ListItem>
          {user.length === 0 ? (
            <CircularProgress color="secondary" />
          ) : isEnable ? (
            <TextField
              id="filled-size-small"
              defaultValue={user}
              size="small"
              InputProps={{
                readOnly: false,
              }}
              onKeyPress={(e) => handleChange(e)}
              onChange={(e) => setUser(e.target.value)}
            />
          ) : (
            <ListItemText primary={user} />
          )}

          <ListItemSecondaryAction>
            <IconButton
              edge="end"
              aria-label="Editar"
              onClick={() => {
                setIsEnable(!isEnable);
              }}
            >
              <GoPencil />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      </List>
      <Divider />
    </div>
  );

  const logout = (
    <div>
      <List>
        <ListItem
          button
          onClick={() => {
            history.push("/");
            localStorage.removeItem("token");
            setAuth(null);
          }}
        >
          <ListItemIcon>
            <IconButton edge="end" aria-label="Logout">
              <BiLogOut />
            </IconButton>
          </ListItemIcon>
          <ListItemText primary={"Logout"} />
        </ListItem>
      </List>
    </div>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <Conteiner>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === "rtl" ? "right" : "left"}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            <MenuContainer>
              {welcomeUser}
              {respMenu}
              {logout}
            </MenuContainer>
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            <MenuContainer>
              {welcomeUser}
              {respMenu}
              {logout}
            </MenuContainer>
          </Drawer>
        </Hidden>
      </nav>
    </Conteiner>
  );
};

Menu.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default Menu;
