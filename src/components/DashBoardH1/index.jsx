import { H1DashBoard } from "./styles";

const H1 = ({ children }) => {
  return (
    <>
      <H1DashBoard> {children}</H1DashBoard>
    </>
  );
};

export default H1;
