import styled from "styled-components";

export const H1DashBoard = styled.h1`
  font-family: "Alegreya Sans SC";
  font-size: 30px;
  font-weight: bold;
  width: 150px;
  @media (min-width: 600px) {
    width: 234px;
  }
`;
