import { ModalCont, closeContainer, buttonContainer } from "../../styles/modal";

import Button from "../Button";
import Input from "../Input";
import CloseIcon from "@material-ui/icons/Close";

import { useAuth } from "../../hooks/Auth";

import { useForm } from "react-hook-form";
import { toast } from "react-toastify";

import { api } from "../../services/api";

const ModalAddGroups = ({ setOpen, getGroup }) => {
  // o setOpen é usado para fechar o modal, então deve ser criado um booleano para usar no handleClick do botão que chama o modal com o ternário.

  const { auth } = useAuth();

  const handleClose = () => {
    setOpen(false);
  };

  const { register, handleSubmit } = useForm();

  const handleGroups = (data) => {
    const newGroupsData = {
      ...data,
      category: `@habitsinfocus/${data.category}`,
    };

    let config = {
      headers: {
        Authorization: `Bearer ${auth}`,
      },
    };
    api
      .post("/groups/", newGroupsData, config)
      .then((response) => {
        toast.success("Cadastro com sucesso");
        getGroup();
      })
      .catch((err) => toast.error("Erro ao Cadastrar"));
  };

  return (
    <ModalCont>
      <div className="conteiner">
        <div style={closeContainer}>
          <CloseIcon
            onClick={handleClose}
            style={{ marginRight: 10, marginTop: 5 }}
          />
        </div>
        <p style={{ width: "80%", margin: "0 auto" }}>Adicionar Grupo:</p>
        <div style={buttonContainer}>
          <form onSubmit={handleSubmit(handleGroups)}>
            <Input
              label="Nome"
              name="name"
              register={register}
              placeholder="Nome Grupo"
            />
            <Input
              label="Descricao"
              name="description"
              register={register}
              placeholder="Descrição resumida"
            />
            <Input
              label="Categoria"
              name="category"
              register={register}
              placeholder="Categoria"
            />
            <Button type="submit"> Adicionar </Button>
          </form>
        </div>
      </div>
    </ModalCont>
  );
};

export default ModalAddGroups;
