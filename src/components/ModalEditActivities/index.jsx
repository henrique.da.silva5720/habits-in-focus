import { ModalCont, closeContainer, buttonContainer } from "../../styles/modal";
import { useState } from "react";

import Button from "../Button";
import CloseIcon from "@material-ui/icons/Close";

const ModalEditActivities = ({ setOpen, callbackUpdate, idActivitie }) => {
  // o setOpen é usado para fechar o modal, então deve ser criado um booleano para usar no handleClick do botão que chama o modal com o ternário.
  // necessário ser chamado  esse modal do id do grupo e do nome do grupo que o goal será criado

  const [input, setInput] = useState("");

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <ModalCont>
      <div>
        <div style={closeContainer}>
          <CloseIcon
            onClick={handleClose}
            style={{ marginRight: 10, marginTop: 5 }}
          />
        </div>
        <p style={{ width: "80%", margin: "0 auto" }}>
          Editar titulo da atividade:
        </p>
        <div style={buttonContainer}>
          <input
            label="Titulo"
            name="title"
            value={input}
            placeholder="Titulo"
            onChange={(e) => setInput(e.target.value)}
          />
          <Button
            onClick={() => {
              callbackUpdate(idActivitie, input);
            }}
          >
            {" "}
            Editar{" "}
          </Button>
        </div>
      </div>
    </ModalCont>
  );
};

export default ModalEditActivities;
