import { Container } from "../../styles/table";
import { IoIosArrowDown } from "react-icons/io";

import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  FormControlLabel,
} from "@material-ui/core";
import { useAuth } from "../../hooks/Auth";
import Button from "../Button";
import { api } from "../../services/api";
import { useState } from "react";
import CircularProgress from "@material-ui/core/CircularProgress";

const Table = ({ habitsList, getHabits }) => {
  const { auth } = useAuth();
  const [progress, setProgress] = useState(false);

  const updateHabit = (id, isTrue) => {
    const newList = habitsList.filter((item) => item.id === id);
    const how_much_achievedForTrue = newList[0].how_much_achieved + 1;
    const how_much_achievedForFalse = newList[0].how_much_achieved - 1;
    return !isTrue ? (
      <>
        {api
          .patch(
            `/habits/${id}/`,
            {
              how_much_achieved: how_much_achievedForTrue,
              achieved: true,
            },
            {
              headers: {
                Authorization: `Bearer ${auth}`,
              },
            }
          )
          .then(() => {
            getHabits();
            setProgress(false);
          })}
      </>
    ) : (
      <>
        {api
          .patch(
            `/habits/${id}/`,
            {
              how_much_achieved: how_much_achievedForFalse,
              achieved: false,
            },
            {
              headers: {
                Authorization: `Bearer ${auth}`,
              },
            }
          )
          .then(() => {
            getHabits();
            setProgress(false);
          })}
      </>
    );
  };
  const deleteHabit = (id) => {
    api
      .delete(`/habits/${id}/`, {
        headers: {
          Authorization: `Bearer ${auth}`,
        },
      })
      .then(() => getHabits())
      .catch((err) => console.log(err));
  };

  return (
    <Container>
      {habitsList &&
        habitsList.map((habit) => (
          <Accordion key={habit.id}>
            <AccordionSummary
              expandIcon={<IoIosArrowDown />}
              aria-label="Expand"
              aria-controls="additional-actions1-content"
              id="additional-actions1-header"
            >
              <FormControlLabel
                aria-label="Acknowledge"
                onClick={(event) => event.stopPropagation()}
                onFocus={(event) => event.stopPropagation()}
                control={
                  <>
                    {!habit.achieved ? (
                      <Button
                        onClick={() => {
                          updateHabit(habit.id, habit.achieved);
                          setProgress(true);
                        }}
                      >
                        Completar
                      </Button>
                    ) : (
                      <>
                        <Button
                          onClick={() => {
                            updateHabit(habit.id, habit.achieved);
                            setProgress(true);
                          }}
                        >
                          Descompletar
                        </Button>
                      </>
                    )}
                  </>
                }
                label={habit.title}
              />
            </AccordionSummary>
            <AccordionDetails>
              <p>
                <b>Categoria:</b> {habit.category}
              </p>
              <p>
                <b>Dificuldade:</b> {habit.difficulty}
              </p>
              <p>
                <b>Frequência:</b> {habit.frequency}
              </p>
              <p>
                <b>Completo:</b> {habit.how_much_achieved}
              </p>
              <p>
                <b>Estado:</b>{" "}
                {habit.achieved === false ? (
                  <span>
                    Não Completo{" "}
                    {progress && <CircularProgress color="secondary" />}
                  </span>
                ) : (
                  <span className="complete">
                    Completo{" "}
                    {progress && <CircularProgress color="secondary" />}
                  </span>
                )}
              </p>
              <Button onClick={() => deleteHabit(habit.id)}>Remover</Button>
            </AccordionDetails>
          </Accordion>
        ))}
    </Container>
  );
};

export default Table;
