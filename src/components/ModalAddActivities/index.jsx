import { ModalCont, closeContainer, buttonContainer } from "../../styles/modal";

import Button from "../Button";
import TextField from "@material-ui/core/TextField";
import CloseIcon from "@material-ui/icons/Close";

import { useAuth } from "../../hooks/Auth";

import { useForm } from "react-hook-form";

import { toast } from "react-toastify";

import { api } from "../../services/api";

import { useGroup } from "../../hooks/Group";

const ModalAddActivities = ({ setOpen, idGroup, nameGrupo }) => {
  // o setOpen é usado para fechar o modal, então deve ser criado um booleano para usar no handleClick do botão que chama o modal com o ternário.
  // necessário ser chamado  esse modal do id do grupo e do nome do grupo que o goal será criado

  const { auth } = useAuth();

  const { getGroupActivities } = useGroup();

  const handleClose = () => {
    setOpen(false);
  };

  const { register, handleSubmit } = useForm();

  const handleGroups = (data) => {
    data.date = `${data.date}:00Z`;

    const newActivitiesData = {
      ...data,
      group: idGroup,
    };

    let config = {
      headers: {
        Authorization: `Bearer ${auth}`,
      },
    };
    api
      .post("/activities/", newActivitiesData, config)
      .then((response) => {
        getGroupActivities(idGroup);
        toast.success("Cadastro com sucesso");
      })
      .catch((err) => toast.error("Erro ao Cadastrar"));
  };

  return (
    <ModalCont>
      <div className="conteiner">
        <div style={closeContainer}>
          <CloseIcon
            onClick={handleClose}
            style={{ marginRight: 10, marginTop: 5 }}
          />
        </div>
        <p style={{ width: "80%", margin: "0 auto" }}>
          Adicionar Atividade ao grupo {nameGrupo}:
        </p>
        <div style={buttonContainer}>
          <form onSubmit={handleSubmit(handleGroups)}>
            <TextField
              margin="normal"
              label="Título"
              InputLabelProps={{ shrink: true }}
              variant="outlined"
              name="title"
              {...register("title")}
            />
            <TextField
              margin="normal"
              label="Data/Hora Realização"
              type="datetime-local"
              InputLabelProps={{ shrink: true }}
              variant="outlined"
              name="realization_time"
              {...register("realization_time")}
            />
            <Button type="submit"> Adicionar </Button>
          </form>
        </div>
      </div>
    </ModalCont>
  );
};

export default ModalAddActivities;
