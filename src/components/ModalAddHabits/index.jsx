import { ModalCont, closeContainer, buttonContainer } from "../../styles/modal";

import Button from "../Button";
import Input from "../Input";
import CloseIcon from "@material-ui/icons/Close";

import { useAuth } from "../../hooks/Auth";

import { useForm } from "react-hook-form";

import { toast } from "react-toastify";

import { api } from "../../services/api";

const ModalAddHabits = ({ setOpen, setHabitsList, habitsList, getHabits }) => {
  // a prop  setOpen é usado para fechar o modal, então deve ser criado um booleano para usar no handleClick do botão que chama o modal com o ternário.
  // a prop setLocalHabitsList é a que foi passada para ter os hábitos locais, ela está sendo setada na função que da post na api.

  const { getUserId, auth } = useAuth();

  const idUser = getUserId();

  const handleClose = () => {
    setOpen(false);
  };

  const { register, handleSubmit } = useForm();

  const handleHabits = (data) => {
    const newData = {
      ...data,
      achieved: false,
      how_much_achieved: 0,
      category: `@habitsinfocus/${data.category}`,
      user: idUser,
    };

    let config = {
      headers: {
        Authorization: `Bearer ${auth}`,
      },
    };
    api
      .post("/habits/", newData, config)
      .then((response) => {
        toast.success("Cadastro com sucesso");
        getHabits();
      })
      .catch((err) => toast.error("Erro ao Cadastrar"));
  };

  return (
    <ModalCont>
      <div className="conteiner">
        <div style={closeContainer}>
          <CloseIcon
            onClick={handleClose}
            style={{ marginRight: 10, marginTop: 5 }}
          />
        </div>
        <p>Adicionar Hábitos:</p>
        <div style={buttonContainer}>
          <form onSubmit={handleSubmit(handleHabits)}>
            <Input
              label="Titulo"
              name="title"
              register={register}
              placeholder="Nome"
            />
            <Input
              label="Categoria"
              name="category"
              register={register}
              placeholder="Categoria"
            />
            <Input
              label="Dificuldade"
              name="difficulty"
              register={register}
              placeholder="Muito dificil"
            />
            <Input
              label="Frequencia"
              name="frequency"
              register={register}
              placeholder="Diária"
            />

            <Button type="submit"> Adicionar </Button>
          </form>
        </div>
      </div>
    </ModalCont>
  );
};

export default ModalAddHabits;
