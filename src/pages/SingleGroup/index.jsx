import TableGroupGoal from "../../components/TableGroupGoal";
import Menu from "../../components/Menu";
import { Container, Infos, LinkNav } from "./styles";
import H1 from "../../components/DashBoardH1";
import ModalAddGoals from "../../components/ModalAddGoals";
import DashboardButton from "../../components/DashboardButton";
import { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import { useGroup } from "../../hooks/Group";
import CircularProgress from "@material-ui/core/CircularProgress";

const SingleGroup = () => {
  const { id } = useParams();

  const {
    currentGroup,
    getCurrentGroup,
    refreshGroupGoals,
    groupGoals,
    getGroupGoals,
    updateGroupGoal,
    deleteGroupGoal,
  } = useGroup();

  const [token] = useState(localStorage.getItem("token") || "");
  const [open, setOpen] = useState(false);

  useEffect(() => {
    getCurrentGroup(id);
    getGroupGoals(id);
    // eslint-disable-next-line
  }, []);

  const handleDelete = (idGoal) => {
    deleteGroupGoal(idGoal, token);
    refreshGroupGoals(idGoal, groupGoals);
  };

  const handleUpdate = (idGoal, concluido) => {
    const config = { achieved: !concluido };
    updateGroupGoal(idGoal, config, token);
    getGroupGoals(id);
  };

  return (
    <>
      <Menu />
      <Container>
        <Infos>
          <H1 children={currentGroup.name} />
          <Link to={`/dashboard/user/group/${id}`}>
            <LinkNav>Metas</LinkNav>
          </Link>
          <Link to={`/dashboard/user/group/activities/${id}`}>
            <LinkNav>Atividades</LinkNav>
          </Link>
          <div>
            <DashboardButton
              funcao={() => setOpen(true)}
              children="Adicionar Metas"
            />
          </div>
        </Infos>
        {open && (
          <ModalAddGoals
            setOpen={setOpen}
            idGroup={id}
            nameGrupo={currentGroup.name}
          />
        )}
        {groupGoals.length === 0 ? (
          <CircularProgress color="secondary" />
        ) : (
          <TableGroupGoal
            content={groupGoals}
            callbackDelete={handleDelete}
            callbackUpdate={handleUpdate}
          />
        )}
      </Container>
    </>
  );
};

export default SingleGroup;
