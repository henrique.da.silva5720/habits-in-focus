import styled from "styled-components";

export const Container = styled.div`
  width: 100%;

  margin-top: 80px;

  display: flex;
  flex-direction: column;

  @media (min-width: 600px) {
    width: 70%;
    margin-left: 300px;
  }
`

export const TableTable = styled.div`
  height: 500px;
  width: 100%;
`

export const Infos = styled.div`
  margin: 0 20px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

export const LinkNav = styled.li`
  font-size: 1rem;
  text-decoration: none;
  color: var(--black);
  

  &:hover {
    color: var(--violet);
  }

  @media (max-width: 767px) {
    font-size: .9rem;
    margin-right: .3rem;
  }
`
