import styled from "styled-components";

import dashboard_desktop from "./../../assets/Dashboard/dashboard_desktop.png";
import dashboard_mobile from "./../../assets/Dashboard/dashboard_mobile.png";

export const ContentBottom = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;

  @media (max-width: 767px) {
    justify-content: center;
    margin-top: 3.2rem;
  }
`;
export const TextBottom = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-top: -5rem;
  margin-bottom: 1rem;

  @media (max-width: 767px) {
    width: 75vw;
  }
`;
export const TitleBottom = styled.h3`
  margin: 0 0;
  font-size: 1.5rem;
  font-weight: 600;

  @media (max-width: 767px) {
    margin-bottom: 1rem;
    font-size: 1.2rem;
  }
`;
export const ParagrafBottom = styled.p`
  font-size: 1rem;
  text-align: center;

  @media (max-width: 767px) {
    width: 75vw;
    font-size: 0.8rem;
  }
`;

export const Sample = styled.div`
  width: 80vw;
  height: 55vh;

  margin: 1rem 0;
  padding: 0 50px;

  background-color: var(--violet);
  border-radius: 0 10rem 0 5rem;

  display: flex;
  align-items: center;
  justify-content: center;

  div {
    width: 80%;
    height: 80%;

    background: url(${dashboard_desktop}) no-repeat center;
    background-size: contain;

    flex: 1;
  }

  @media (max-width: 767px) {
    padding: 0 30px;

    border-radius: 0 5rem 0 3rem;

    div {
      width: 80%;
      height: 80%;

      background: url(${dashboard_mobile}) no-repeat center;
      background-size: contain;

      flex: 1;
    }
  }
`;
export const NavBar = styled.nav`
  width: 100vw;
  height: 10vh;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: var(--violet);

  @media (max-width: 767px) {
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 15vh;
  }
`;
export const UpLogo = styled.img`
  width: 20rem;
  margin-left: 4.5rem;
  margin-top: 1rem;

  @media (max-width: 767px) {
    width: 15rem;
    margin-left: 0;
  }
`;
export const ContentBtn = styled.div`
  width: 10rem;
  display: flex;
  justify-content: space-around;
  margin-right: 2rem;

  @media (max-width: 767px) {
    margin-right: 0;
  }
`;
export const ButtonLogin = styled.button`
  width: 4.2rem;
  background-color: transparent;
  border: none;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 0.8rem;

  &:hover {
    color: var(--white);
  }
`;
export const ButtonSigUp = styled.button`
  border: none;
  border-radius: 4px;
  padding: 0.2rem;
  width: 4.2rem;
  font-size: 0.8rem;

  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ContentTop = styled.div`
  background-color: var(--violet);
  display: flex;
  justify-content: space-around;
  align-items: center;
  width: 100vw;

  @media (max-width: 767px) {
    flex-direction: column;
  }
`;
export const Text = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  @media (max-width: 767px) {
    width: 75vw;
  }
`;
export const TitleTop = styled.h2`
  font-size: 2rem;
  margin-bottom: 1rem;
  border-top: 4px;
  @media (max-width: 769px) {
    margin-top: 0.5rem;
    font-size: 1.5rem;
  }
`;
export const ParagrafTop = styled.p`
  width: 30vw;
  font-size: 1.2rem;
  text-overflow: clip;

  @media (max-width: 767px) {
    width: 75vw;
    font-size: 1rem;
  }
`;
export const ImageTop = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  max-width: 500px;
  width: 40vw;

  @media (max-width: 767px) {
    margin-top: 0;
    margin-right: 0;
    height: 10rem;
    width: 15rem;
  }
`;

export const Footer = styled.div`
  padding: 30px;
  width: 100vw;
  background-color: var(--black);
  margin-top: 1rem;

  display: flex;
  align-items: center;
  justify-content: space-around;

  @media (max-width: 767px) {
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
`;
export const Logo = styled.img`
  width: 15rem;
`;
export const FlexFooter = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  width: 70vw;

  @media (max-width: 767px) {
    display: flex;
    justify-content: space-around;
    align-items: center;
  }
`;

export const Column = styled.div`
  color: var(--white);
  width: 15;
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  @media (max-width: 767px) {
    margin: 0.3rem 1rem;
  }
`;
export const Item = styled.li`
  list-style: none;
  text-decoration: none;
  color: var(--white);
  font-size: 0.7rem;
  margin-top: 0.1rem;
  text-align: center;

  display: flex;
  align-items: center;
  justify-content: center;
`;
export const List = styled.ul`
  display: flex;
  flex-direction: column;
`;
