import { BiLogIn, BiPhone } from "react-icons/bi";
import { IoArrowRedoOutline } from "react-icons/io5";
import {
  NavBar,
  UpLogo,
  ContentBtn,
  ButtonLogin,
  ButtonSigUp,
  Footer,
  FlexFooter,
  Column,
  Item,
  List,
  Logo,
  ContentTop,
  Text,
  TitleTop,
  ParagrafTop,
  ImageTop,
  ContentBottom,
  TextBottom,
  TitleBottom,
  ParagrafBottom,
  Sample,
} from "./styles";
import { useHistory } from "react-router-dom";
import LogoRoxo from "../../assets/Logo/LogoRoxo.png";
import LogoPreto from "../../assets/Logo/LogoPreto.png";

import Button from "../../components/Button";
import { Player } from "@lottiefiles/react-lottie-player";
import {
  AiOutlineMail,
  AiOutlineInstagram,
  AiOutlineCopyrightCircle,
  AiOutlineTwitter,
} from "react-icons/ai";

const Home = () => {
  const history = useHistory();
  const handleChangePage = (path) => history.push(path);

  return (
    <>
      <NavBar>
        <UpLogo src={LogoRoxo} alt="LogoApp" />
        <ContentBtn>
          <ButtonLogin onClick={() => handleChangePage("/login")}>
            <BiLogIn />
            Login
          </ButtonLogin>
          <ButtonSigUp onClick={() => handleChangePage("/signup")}>
            <IoArrowRedoOutline />
            Signup
          </ButtonSigUp>
        </ContentBtn>
      </NavBar>
      <ContentTop>
        <Text>
          <TitleTop>Lorem Ipsum</TitleTop>
          <ParagrafTop>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. scrambled it to make a type specimen book. It has survived
            not only five centuries, but also the leap into electronic
            typesetting, remaining essentially unchanged.
          </ParagrafTop>
        </Text>
        <ImageTop>
          <Player
            autoplay
            loop
            style={{ height: "100%", width: "100%" }}
            src="https://assets9.lottiefiles.com/packages/lf20_bpqri9y8.json"
          />
        </ImageTop>
      </ContentTop>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path
          fill="#A293FF"
          fill-opacity="1"
          d="M0,64L120,85.3C240,107,480,149,720,149.3C960,149,1200,107,1320,85.3L1440,64L1440,0L1320,0C1200,0,960,0,720,0C480,0,240,0,120,0L0,0Z"
        ></path>
      </svg>
      <ContentBottom>
        <TextBottom>
          <TitleBottom>Lorem Ipsum</TitleBottom>
          <ParagrafBottom>
            it's more than work. It's a way of working together.
          </ParagrafBottom>
        </TextBottom>
        <Button onClick={() => handleChangePage("/signup")}>
          Faça seu cadastro
        </Button>
        <Sample>
          <div className="image" />
        </Sample>
      </ContentBottom>
      <Footer>
        <Column>
          <Logo src={LogoPreto} alt="LogoApp" />
          <Item>Termos e Privacidade</Item>
          <Item>
            <AiOutlineCopyrightCircle size=".5rem" />
            2021 Habits in Focus Inc.
          </Item>
        </Column>
        <FlexFooter>
          <Column>
            Links
            <List>
              <Item>Home</Item>
              <Item>Login</Item>
              <Item>Signup</Item>
            </List>
          </Column>
          <Column>
            Ajuda e Contatos
            <List>
              <Item>Ajuda Suporte</Item>
              <Item>
                <AiOutlineMail size=".6rem" /> E-mail
              </Item>
              <Item>
                <AiOutlineInstagram size=".6rem" /> Instagram
              </Item>
              <Item>
                <AiOutlineTwitter size=".6rem" /> Twitter
              </Item>
              <Item>
                <BiPhone size=".6rem" /> Contato
              </Item>
            </List>
          </Column>
          <Column>
            {" "}
            Outros
            <List>
              <Item>Feedback</Item>
              <Item>FAQ</Item>
              <Item>Time</Item>
            </List>
          </Column>
        </FlexFooter>
      </Footer>
    </>
  );
};

export default Home;
