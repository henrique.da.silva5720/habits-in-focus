import styled from "styled-components";

export const Container = styled.div`
  width: 100%;

  margin-top: 80px;

  display: flex;
  flex-direction: column;

  @media (min-width: 600px) {
    width: 70%;
    margin-left: 300px;
  }
`;

export const Infos = styled.div`
  margin: 0 20px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const ContainerTable = styled.div`
  > div:nth-child(1) {
    margin-bottom: 10px;
  }

  > div:nth-child(2) {
    max-width: 350px;

    margin: 0 auto 10px;

    display: flex;
    justify-content: space-between;
  }
`;
