import Menu from "../../components/Menu";
import { Container, Infos, ContainerTable } from "./style";
import { api } from "../../services/api";
import { useEffect, useState } from "react";
import ModalAddGroups from "../../components/ModalAddGroups";
import TableGroupEdit from "../../components/TableGroup";
import DashboardButton from "../../components/DashboardButton";
import H1 from "../../components/DashBoardH1";
import { useAuth } from "../../hooks/Auth";
import CircularProgress from "@material-ui/core/CircularProgress";

const DashboardGroupCreated = () => {
  const [group, setGroup] = useState([]);
  const [openModal, setOpenModal] = useState(false);
  const { auth } = useAuth();

  let config = {
    headers: {
      Authorization: `Bearer ${auth}`,
    },
  };

  const getGroups = () => {
    api.get("/groups/subscriptions/", config).then((res) => {
      setGroup(
        res.data.map(
          (x) =>
            (x = { ...x, category: x.category.replace("@habitsinfocus/", "") })
        )
      );
    });
  };

  useEffect(() => {
    getGroups();
    // eslint-disable-next-line
  }, []);

  const handleOpenModal = () => {
    setOpenModal(true);
  };

  return (
    <>
      <Menu />
      <Container>
        <Infos>
          <H1 children="Meus Grupos" />
          <DashboardButton
            children="Adicionar Grupos"
            funcao={handleOpenModal}
          />
        </Infos>
        <ContainerTable>
          {group.length === 0 ? (
            <CircularProgress color="secondary" />
          ) : (
            <TableGroupEdit
              groupList={group}
              name="Editar"
              getGroupAction={getGroups}
            />
          )}
        </ContainerTable>
        {openModal && (
          <ModalAddGroups setOpen={setOpenModal} getGroup={getGroups} />
        )}
      </Container>
    </>
  );
};

export default DashboardGroupCreated;
