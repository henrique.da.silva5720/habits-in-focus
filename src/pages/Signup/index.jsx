import FormRegister from "../../components/FormRegister";
import {
  ContainerForm,
  ContainerGeral,
  ContainerImage,
  BackgroundBall,
} from "../../styles/formPages";
import { Link } from "react-router-dom";
import { Player } from "@lottiefiles/react-lottie-player";
import { useHistory } from "react-router";
import { useAuth } from "../../hooks/Auth";

const Register = () => {
  const { auth } = useAuth();

  const history = useHistory();

  if (auth) {
    history.push("/dashboard/user");
  }

  return (
    <ContainerGeral>
      <BackgroundBall top />
      <ContainerImage>
        <Player
          src="https://assets2.lottiefiles.com/packages/lf20_t6nmtpwm.json"
          autoplay
          loop
        />
      </ContainerImage>
      <ContainerForm>
        <FormRegister />
        <span>
          Já tem uma conta? Faça <Link to="/login">login</Link>
        </span>
      </ContainerForm>
    </ContainerGeral>
  );
};

export default Register;
