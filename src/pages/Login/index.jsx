import FormLogin from "../../components/FormLogin";
import { Link } from "react-router-dom";
import {
  ContainerForm,
  ContainerGeral,
  ContainerImage,
  BackgroundBall,
} from "../../styles/formPages";
import { Player } from "@lottiefiles/react-lottie-player";

import { useAuth } from "../../hooks/Auth";
import { useHistory } from "react-router";
const Login = () => {
  const { auth } = useAuth();
  const history = useHistory();
  if (auth) {
    history.push("/dashboard/user");
  }
  return (
    <ContainerGeral>
      <ContainerForm>
        <FormLogin />
        <span>
          Não tem conta? Faça seu <Link to="/signup">Cadastro</Link>
        </span>
      </ContainerForm>
      <BackgroundBall bottom />
      <ContainerImage>
        <Player
          src="https://assets2.lottiefiles.com/packages/lf20_jcikwtux.json"
          style={{
            height: "500px",
            width: "500px",
          }}
          autoplay
          loop
        ></Player>
      </ContainerImage>
    </ContainerGeral>
  );
};

export default Login;
