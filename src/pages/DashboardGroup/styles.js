import styled from "styled-components";

export const Container = styled.div`
  width: 100%;

  margin-top: 80px;

  display: flex;
  flex-direction: column;

  @media (min-width: 600px) {
    width: 70%;
    margin-left: 300px;
  }
`;

export const ContainerHeader = styled.div`
  margin-bottom: 10px;

  display: flex;
  justify-content: space-between;
  align-items: center;

  h1 {
    margin-right: 20px;

    font-size: 30px;
  }
`;

export const ContainerTable = styled.div`
  > div:nth-child(1) {
    margin-bottom: 10px;
  }

  > div:nth-child(2) {
    max-width: 350px;

    margin: 0 auto 10px;

    display: flex;
    justify-content: space-between;

    button {
      height: 40px;
      border-radius: 20px;
    }
  }
`;

export const Input = styled.div`
  width: 100%;
  max-width: 300px;
  padding: 10px;

  background-color: var(--white);
  border: 2px solid var(--black);
  border-radius: 5px;

  display: flex;

  transition: 0.4s;

  svg {
    margin-right: 10px;
  }

  input {
    background: transparent;
    border: none;
    width: 100%;
    align-items: center;
    flex: 1;
  }
`;
