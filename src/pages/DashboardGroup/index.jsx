import { Container, ContainerHeader, ContainerTable, Input } from "./styles";
import Button from "./../../components/Button";
import H1 from "../../components/DashBoardH1";

import Menu from "./../../components/Menu";
import { BiSearchAlt } from "react-icons/bi";

import { useState } from "react";

import { useEffect } from "react";

import { api } from "../../services/api";
import TableGroup from "../../components/TableGroupSearch";
import CircularProgress from "@material-ui/core/CircularProgress";

const DashboardGroup = () => {
  const [groups, setGroups] = useState([]);

  const [userInput, setUserInput] = useState("");
  const [groupList, setGroupList] = useState(groups);

  const [page, setPage] = useState(1);
  const [finalPage, setFinalPage] = useState("");

  useEffect(() => {
    api
      .get(`/groups/?page=${page}&category=%40habitsinfocus%2F`)
      .then((res) => {
        let data = res.data;
        data.results = data.results.map((item) => {
          const categoriaFormatada = item.category.replace(
            "@habitsinfocus/",
            ""
          );
          const output = {
            ...item,
            category: categoriaFormatada,
          };

          return output;
        });
        setFinalPage(res.data.next);
        setGroups(data.results);
      })
      .catch((err) => console.log(err));
  }, [page]);

  const handleChange = (event) => {
    setUserInput(event.target.value);
  };

  useEffect(() => {
    const search = groups.filter((group) =>
      group.name.toLowerCase().includes(userInput.toLowerCase())
    );
    setGroupList(search);
  }, [userInput, groups]);

  const previewPage = () => {
    if (page > 1) {
      setPage(page - 1);
    }
  };

  const nextPage = () => {
    if (finalPage !== null) {
      setPage(page + 1);
    }
  };

  return (
    <>
      <Menu />
      <Container>
        <ContainerHeader>
          <H1>Buscar Grupos</H1>
          <Input>
            <BiSearchAlt size={20} />
            <input value={userInput} onChange={handleChange} />
          </Input>
        </ContainerHeader>
        <ContainerTable>
          {groupList.length === 0 ? (
            <CircularProgress color="secondary" />
          ) : (
            <TableGroup groupList={groupList} name="Entrar" />
          )}
          <div>
            <Button onClick={previewPage}>Voltar</Button>
            <Button onClick={nextPage}>Avançar</Button>
          </div>
        </ContainerTable>
      </Container>
    </>
  );
};

export default DashboardGroup;
