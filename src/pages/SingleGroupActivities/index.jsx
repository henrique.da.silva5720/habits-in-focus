import TableGroupActivities from "../../components/TableGroupActivities";
import Menu from "../../components/Menu";
import { Container, Infos, LinkNav } from "./styles";
import H1 from "../../components/DashBoardH1";
import DashboardButton from "../../components/DashboardButton";
import ModalAddActivities from "../../components/ModalAddActivities";
import { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import { useGroup } from "../../hooks/Group";
import CircularProgress from "@material-ui/core/CircularProgress";

const SingleGroupActivities = () => {
  const { id } = useParams();

  const [open, setOpen] = useState(false);

  const {
    currentGroup,
    getCurrentGroup,
    refreshGroupActivities,
    groupActivities,
    getGroupActivities,
    updateGroupActivities,
    deleteGroupActivities,
  } = useGroup();

  const [token] = useState(localStorage.getItem("token") || "");

  useEffect(() => {
    getCurrentGroup(id);
    getGroupActivities(id);
    // eslint-disable-next-line
  }, []);

  const handleDelete = (idGoal) => {
    deleteGroupActivities(idGoal, token);
    refreshGroupActivities(idGoal, groupActivities);
  };

  const handleUpdate = (idGoal, titulo) => {
    const config = { title: titulo };
    updateGroupActivities(idGoal, config, token);
    getGroupActivities(id);
  };

  return (
    <>
      <Menu />
      <Container>
        <Infos>
          <H1 children={currentGroup.name} />
          <Link to={`/dashboard/user/group/${id}`}>
            <LinkNav>Metas</LinkNav>
          </Link>
          <Link to={`/dashboard/user/group/activities/${id}`}>
            <LinkNav>Atividades</LinkNav>
          </Link>
          <DashboardButton
            funcao={() => setOpen(true)}
            children="Adicionar Atividades"
          />
        </Infos>
        {open && (
          <ModalAddActivities
            setOpen={setOpen}
            idGroup={id}
            nameGrupo={currentGroup.name}
          />
        )}
        {groupActivities.length === 0 ? (
          <CircularProgress color="secondary" />
        ) : (
          <TableGroupActivities
            content={groupActivities}
            callbackDelete={handleDelete}
            callbackUpdate={handleUpdate}
          />
        )}
      </Container>
    </>
  );
};

export default SingleGroupActivities;
