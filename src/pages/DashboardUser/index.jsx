import Table from "../../components/TableUser";
import Menu from "../../components/Menu";
import { Container, TableTable, Infos } from "./styles";
import H1 from "../../components/DashBoardH1";
import DashboardButton from "../../components/DashboardButton";
import { useState } from "react";
import { api } from "./../../services/api";
import { useEffect } from "react";
import { useAuth } from "../../hooks/Auth";
import ModalAddHabits from "../../components/ModalAddHabits";
import CircularProgress from "@material-ui/core/CircularProgress";

const DashboardPage = () => {
  const { auth } = useAuth();
  const [habitsList, setHabitsList] = useState([]);
  const [open, setOpen] = useState(false);

  const getHabits = () => {
    api
      .get("/habits/personal/", {
        headers: { Authorization: `Bearer ${auth}` },
      })
      .then((res) => setHabitsList(res.data))
      .catch((e) => console.log(e));
  };

  useEffect(() => {
    getHabits();
    // eslint-disable-next-line
  }, []);

  const handleOpen = () => {
    setOpen(true);
  };

  return (
    <>
      <Menu />
      <Container>
        <Infos>
          <H1 children="Meus hábitos" />
          <DashboardButton children="Adicionar Hábitos" funcao={handleOpen} />
        </Infos>
        <TableTable>
          {habitsList.length === 0 ? (
            <CircularProgress color="secondary" />
          ) : (
            <Table habitsList={habitsList} getHabits={getHabits} />
          )}
        </TableTable>
        {open && <ModalAddHabits setOpen={setOpen} getHabits={getHabits} />}
      </Container>
    </>
  );
};

export default DashboardPage;
