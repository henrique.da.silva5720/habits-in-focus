import styled from "styled-components";

export const Container = styled.div`
  width: 100%;

  margin-top: 80px;

  display: flex;
  flex-direction: column;

  @media (min-width: 600px) {
    width: 70%;
    margin-left: 300px;
  }
`;

export const TableTable = styled.div`
  height: 500px;
  width: 100%;
`;

export const Infos = styled.div`
  margin: 0 20px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
