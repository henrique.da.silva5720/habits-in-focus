import { createContext, useContext, useState } from "react";
import { api } from "../../services/api";

const GroupContext = createContext();

export const GroupProvider = ({ children }) => {
  const [currentGroup, setCurrentGroup] = useState(0);
  const [groupGoals, setGroupGoals] = useState([]);
  const [groupActivities, setGroupActivities] = useState([]);

  const getCurrentGroup = (id) => {
    api
      .get(`/groups/${id}/`)
      .then((response) => setCurrentGroup(response.data));
  };

  const getGroupGoals = (id) => {
    api
      .get(`/goals/?group=${id}`)
      .then((response) => setGroupGoals(response.data.results));
  };

  const refreshGroupGoals = (id, listGoals) => {
    const filter = listGoals.filter((item) => item.id !== id);
    setGroupGoals(filter);
  };

  const createGroupGoal = (body, token) => {
    api.post(`/goals/`, body, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  };

  const updateGroupGoal = (id, body, token) => {
    api.patch(`/goals/${id}/`, body, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  };

  const deleteGroupGoal = (id, token) => {
    api.delete(`/goals/${id}/`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  };

  const getGroupActivities = (id) => {
    api
      .get(`/activities/?group=${id}`)
      .then((response) => setGroupActivities(response.data.results));
  };

  const refreshGroupActivities = (id, listActivities) => {
    const filter = listActivities.filter((item) => item.id !== id);
    setGroupActivities(filter);
  };

  const createGroupActivities = (body, token) => {
    api.post(`/activities/`, body, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  };

  const updateGroupActivities = (id, body, token) => {
    api.patch(`/activities/${id}/`, body, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  };

  const deleteGroupActivities = (id, token) => {
    api.delete(`/activities/${id}/`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  };

  return (
    <GroupContext.Provider
      value={{
        currentGroup,
        getCurrentGroup,
        groupGoals,
        groupActivities,
        getGroupGoals,
        refreshGroupGoals,
        createGroupGoal,
        updateGroupGoal,
        deleteGroupGoal,
        getGroupActivities,
        refreshGroupActivities,
        createGroupActivities,
        updateGroupActivities,
        deleteGroupActivities,
      }}
    >
      {children}
    </GroupContext.Provider>
  );
};

export const useGroup = () => useContext(GroupContext);
