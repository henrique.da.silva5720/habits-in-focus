import { createContext, useContext, useState } from "react";
import jwt_decode from "jwt-decode";

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [auth, setAuth] = useState(localStorage.getItem("token") || "");

  const getUserId = () => {
    let token = localStorage.getItem("token");
    let decoded = jwt_decode(token);
    return decoded.user_id;
  };

  return (
    <AuthContext.Provider
      value={{
        setAuth,
        getUserId,
        auth,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
