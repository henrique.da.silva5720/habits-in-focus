import { AuthProvider } from "./Auth";
import { GroupProvider } from "./Group";

const Providers = ({ children }) => {
  return (
    <AuthProvider>
      <GroupProvider>
        {children}
      </GroupProvider>
    </AuthProvider>
  );
};

export default Providers;
