import styled from "styled-components";

export const Container = styled.div`
  min-width: 300px;
  width: 100%;
  padding: 5px;
  background-color: var(--gray);

  button {
    width: 100px;
    height: 30px;
    margin-right: 10px;

    color: var(--white);
    font-size: 0.8rem;

    background-color: var(--violet);
    border-radius: 20px;
  }

  > div {
    transition: 0.3s;

    div:nth-child(1) {
      div:nth-child(2) {
        background-color: var(--white);
      }
    }

    div:nth-child(2) {
      background-color: #f3f3f3;

      transition: 0.3s;

      div {
        display: flex;
        flex-direction: column;

        span {
          color: var(--red);
        }

        .complete {
          color: var(--green);
        }

        p + p {
          margin-top: 3px;
        }

        button {
          align-self: center;

          margin-top: 3px;

          background-color: var(--red);
          border-color: var(--red);
        }
      }
    }

    :hover {
      background-color: var(--violet);

      button {
        color: var(--violet);

        background-color: var(--white);
      }

      div:nth-child(1) {
        div:nth-child(2) {
          background-color: var(--violet);
        }
      }

      div:nth-child(2) {
        background-color: #d0c9ff;

        div {
          button {
            color: var(--red);

            background-color: var(--white);
          }
        }
      }
    }
  }
`;
