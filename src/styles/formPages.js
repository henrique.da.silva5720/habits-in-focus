import styled from "styled-components";
export const ContainerForm = styled.div`
  min-width: 256px;
  width: 80%;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  h1 {
    font-size: 2.5rem;
  }

  form {
    button {
      max-width: 500px;
    }

    @media only screen and (min-width: 426px) {
      width: 80%;
    }
  }

  span {
    margin-top: 5px;
  }

  @media only screen and (min-width: 1024px) {
    width: 50%;
  }
`;
export const ContainerGeral = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: space-around;
  overflow: hidden;
  position: relative;
`;
export const ContainerImage = styled.div`
  display: none;

  div div {
    width: 500px;

    @media only screen and (min-width: 1440px) {
      width: 700px;
    }
  }

  @media only screen and (min-width: 1024px) {
    width: 50%;

    display: flex;
    justify-content: center;
    align-items: center;
  }
`;
export const BackgroundBall = styled.div`
  display: none;

  @media only screen and (min-width: 1024px) {
    display: block;

    width: 1400px;
    height: 1400px;
    background-color: #a293ff;
    border-radius: 50%;

    position: absolute;

    top: ${(prop) => prop.top && "-45%"};
    right: ${(prop) => prop.top && "48%"};
    left: ${(prop) => prop.bottom && "48%"};
    bottom: ${(prop) => prop.bottom && "-45%"};
  }
`;
