import styled from "styled-components";

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;

  h1 {
    font-size: 20px;
    font-family: "Alegreya Sans SC";
    font-weight: 700;
    margin-bottom: 20px;
  }

  input {
    width: 200;
    font-family: "Roboto";
  }
  button {
    width: 100%;
    height: 50px;
    margin-top: 30px;
    font-family: "Alegreya Sans SC";
  }
  @media (min-width: 1000px) {
    input {
      width: 340px;
    }
  }
`;
