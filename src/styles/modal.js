import styled from "styled-components";

export const ModalCont = styled.div`
  width: 100%;
  height: 100%;
  z-index: 1900;
  position: fixed;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.75);
  display: flex;
  justify-content: center;
  align-items: center;

  div {
    background-color: #fff;
    color: #000;
    border-radius: 15px;
  }

  .conteiner {
    border: 2px solid #a293ff;
    width: 21vw;
    min-width: 286px;
  }

  p {
    width: 80%;
    margin: 0 auto;
    font-size: 1.5rem;
    text-align: center;
  }

  form {
    display: flex;
    flex-direction: column;
  }
  button {
    align-self: center;
    margin-bottom: 1rem;
  }
`;

export const closeContainer = {
  display: "flex",
  width: "100%",
  justifyContent: "flex-end",
  border: "none",
};

export const buttonContainer = {
  display: "flex",
  width: "100%",
  flexDirection: "column",
  alignItems: "center",
  border: "none",
};
export const Button = styled.button``;
