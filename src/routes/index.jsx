import { Switch, Route } from "react-router-dom";
import DashboardPage from "../pages/DashboardUser";
import Login from "../pages/Login";
import Register from "../pages/Signup";
import { useAuth } from "../hooks/Auth";
import Home from "../pages/Home";
import SingleGroup from "../pages/SingleGroup"
import SingleGroupActivities from "../pages/SingleGroupActivities"
import DashboardGroup from "../pages/DashboardGroup";
import DashboardGroupCreated from "../pages/DashboardGroupCreated";

const Routes = () => {
  const { auth } = useAuth();

  return (
    <Switch>
      <Route exact path="/dashboard"></Route>
      <Route exact path="/login">
        <Login />
      </Route>
      <Route exact path="/signup">
        <Register />
      </Route>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/dashboard/search">
        <DashboardGroup />
      </Route>
      {auth ? (
        <>
          <Route exact path="/dashboard/user">
            <DashboardPage />
          </Route>
          <Route exact path="/dashboard/user/group/:id"> 
            <SingleGroup />
          </Route>
          <Route exact path="/dashboard/user/group/activities/:id"> 
            <SingleGroupActivities />
          </Route>
          <Route exact path="/dashboard/groups">
            <DashboardGroupCreated />
          </Route>
        </>
      ) : (
        <Route path="*">
          <Home />
        </Route>
      )}
      <Route path="*">
        <Home />
      </Route>
    </Switch>
  );
};

export default Routes;
